﻿/*Copyright (C) 2020 MacNokker (macnokker@gmail.com)

This library is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this library; if not, see <https://www.gnu.org/licenses>.

Additional permission under GNU GPL version 3 section 7

If you modify this library, or any covered work, by linking or combining it with "Taleworlds.Core", "TaleWorlds.CampaignSystem", "TaleWorlds.Library", TaleWorlds.InputSystem and "TaleWorlds.MountAndBlade" (or modified versions of these libraries), containing parts covered by the terms of their applicable licenses, the licensors of this library grant you additional permission to convey the resulting work. 
*/
using HarmonyLib;
using System;
using System.Globalization;
using System.IO;
using System.Xml;
using TaleWorlds.Core;
using TaleWorlds.CampaignSystem;
using TaleWorlds.Library;
using TaleWorlds.MountAndBlade;
using TaleWorlds.InputSystem;

namespace HoldIt
{
 	public class TalkFin: CampaignBehaviorBase
	 {

		public override void RegisterEvents()
		{
			// register encounter cleanup event
			CampaignEvents.ConversationEnded.AddNonSerializedListener(this, new Action<CharacterObject>(this.close));
		}

		public override void SyncData(IDataStore dataStore)
		{
		}

		// perform cleanup
		private void close(CharacterObject _)
		{
			if (HIValues.Instance.Created_Encounter != null)
			{
				PlayerEncounter.Finish(false);
				HIValues.Instance.Created_Encounter = null;
			}
		}
	 }



	public class HIValues
	{
		public bool Base_Inited { get; set; } = false;

		public MobileParty PlayerTargetParty { get; set; } = null;

		public PlayerEncounter Created_Encounter { get; set; } = null;
		public bool MsgPrinted {get; set;} = false;
		public bool ShowMessage {get; set;} = false;

		public InputKey TalkKey { get; set; } = InputKey.T;
		public InputKey CourtRecord { get; set; } = InputKey.N;
		public float SimRoundsMultFollow { get; set; } = 0F;
		public float SimRoundsMultAll { get; set; } = 1F;


		public string Cfg_Error { get; set; } = "";

		public void HIInit(string CFGPath = "")
		{

			if (!string.IsNullOrWhiteSpace(CFGPath))
			{
				XmlDocument xd = new XmlDocument();
				xd.Load(CFGPath);

				XmlNode node = xd.SelectSingleNode("/config/base");

				if (node == null)
				{
					Cfg_Error = ": Base Config Node Not Found";
				}
				else
				{
					foreach (XmlNode child_node in node.ChildNodes)
					{

						if (!child_node.HasChildNodes)
						{
							continue;
						}
						if (child_node.FirstChild.NodeType != XmlNodeType.Text || child_node.ChildNodes.Count != 1)
						{
							continue;
						}

						switch (child_node.Name.Trim().ToLower())
						{

							case "simroundsmultall":
								SimRoundsMultAll = float.Parse(child_node.FirstChild.Value, CultureInfo.InvariantCulture.NumberFormat);
								break;
							case "simroundsmultfollow":
								SimRoundsMultFollow = float.Parse(child_node.FirstChild.Value, CultureInfo.InvariantCulture.NumberFormat);
								break;
							case "showmessage":
								ShowMessage = bool.Parse(child_node.FirstChild.Value);
								break;
							case "talkkey":
								TalkKey = (InputKey)Enum.Parse(typeof(InputKey),child_node.FirstChild.Value, true);
								break;
							case "courtrecord":
								CourtRecord = (InputKey)Enum.Parse(typeof(InputKey),child_node.FirstChild.Value, true);
								break;
						}
					}
						Base_Inited = true;
				}
			}
		}

		private static HIValues _instance = null;
		public static HIValues Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new HIValues();
				}
				return _instance;
			}
		}


	}



	public class HoldIt : MBSubModuleBase
	{

		protected override void OnSubModuleLoad()
		{
			base.OnSubModuleLoad();
			string CFGPath = string.Concat(BasePath.Name, "Modules/HoldIt/HIconfig.xml");

			if (File.Exists(CFGPath))
			{
				HIValues.Instance.HIInit(CFGPath);
			}

			try
			{
				var h = new Harmony("cia.bane.rlord.holdit");
				//Harmony.DEBUG = true;
				h.PatchAll();
			}
			catch (Exception exception)
			{
				string message;
				Exception innerException = exception.InnerException;
				if (innerException != null)
				{
					message = innerException.Message;
				}
				else
				{
					message = null;
				}
				FileLog.Log(message);
			}

		}

		// register cleanup behaviour
		protected override void OnGameStart (Game game, IGameStarter gameStarterObject)
		{
			base.OnGameStart(game, gameStarterObject);
			if (game.GameType is Campaign)
			{
				((CampaignGameStarter)gameStarterObject).AddBehavior(new TalkFin());
			}

		}

	}
}
