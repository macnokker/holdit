﻿/*Copyright (C) 2020 MacNokker (macnokker@gmail.com)

This library is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this library; if not, see <https://www.gnu.org/licenses>.

Additional permission under GNU GPL version 3 section 7

If you modify this library, or any covered work, by linking or combining it with "Taleworlds.Core", "TaleWorlds.CampaignSystem", "TaleWorlds.Library", TaleWorlds.InputSystem and "TaleWorlds.MountAndBlade" (or modified versions of these libraries), containing parts covered by the terms of their applicable licenses, the licensors of this library grant you additional permission to convey the resulting work. 
*/
using HarmonyLib;
using System;
using System.Collections.Generic;
using TaleWorlds.CampaignSystem;
using TaleWorlds.Core;
using TaleWorlds.InputSystem;

namespace HoldIt.Patch
{
	[HarmonyPatch(typeof(MapEvent))]
	class CustomSBD
	{
		[HarmonyPrefix]
		[HarmonyPriority(Priority.Low)]
		[HarmonyPatch("SimulateBattleForRounds")]
		static void PreFix(MapEvent __instance, ref int simulationRoundsDefender, ref int simulationRoundsAttacker)
		{
			if (HIValues.Instance.PlayerTargetParty != null)
			{
				// get list of parties in map event and see if stored party is in it
				using (IEnumerator<PartyBase> enumerator = __instance.InvolvedParties.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						if (enumerator.Current == HIValues.Instance.PlayerTargetParty.Party)
						{
							int roundsDefender = (int)(simulationRoundsDefender * HIValues.Instance.SimRoundsMultFollow);
							int RoundsAttacker = (int)(simulationRoundsAttacker * HIValues.Instance.SimRoundsMultFollow);
							simulationRoundsDefender = Math.Max(roundsDefender, 0);
							simulationRoundsAttacker = Math.Max(RoundsAttacker, 0);
							return;
						}
					}
				}

			}

			// apply global mult if configured
			if (HIValues.Instance.SimRoundsMultAll != 1F)
			{
				int simulationRoundsDefender_new = (int)(simulationRoundsDefender * HIValues.Instance.SimRoundsMultAll);
				int simulationRoundsAttacker_new = (int)(simulationRoundsAttacker * HIValues.Instance.SimRoundsMultAll);

				// FileLog.Log("SimRounds Defender " + simulationRoundsDefender + "chng to: " + simulationRoundsDefender_new);
				// FileLog.Log("SimRounds Attacker " + simulationRoundsAttacker + "chng to: " + simulationRoundsAttacker_new);

				simulationRoundsDefender = Math.Max(simulationRoundsDefender_new, 1);
				simulationRoundsAttacker = Math.Max(simulationRoundsAttacker_new, 1);
			}
		}
	}


	[HarmonyPatch(typeof(MobileParty))]
	class PartyState
	{
		[HarmonyPostfix]
		[HarmonyPriority(Priority.Low)]
		[HarmonyPatch("DoAIMove")]
		static void DoAIMove(MobileParty __instance)
		{
			if (__instance.IsMainParty)
			{

				AiBehavior aiBehavior = __instance.DefaultBehavior;
				//InformationManager.DisplayMessage(new InformationMessage("Test Reading:" + moveModeType + "   "+ aiBehavior));

				// if player is following other party
				if (aiBehavior == AiBehavior.EngageParty)
				{
					MobileParty target_party = __instance.TargetParty;
					if (target_party != null)
					{
						// target in map event
						MapEvent tp_event = target_party.MapEvent;
						if (tp_event != null)
						{
							// store target_party
							HIValues.Instance.PlayerTargetParty = target_party;
							if (!HIValues.Instance.MsgPrinted)
							{
								HIValues.Instance.MsgPrinted = true;
								if (HIValues.Instance.ShowMessage)
								{
									InformationManager.DisplayMessage(new InformationMessage("Hold It!"));
								}
							}
							return;
						}
						// target is no longer in a map event
						else 
						{
							if (HIValues.Instance.MsgPrinted)
							{
								HIValues.Instance.MsgPrinted = false;
							}

							// if key is pressed create a new encounter and trigger conversation
							if (Input.IsKeyDown(HIValues.Instance.TalkKey) && PlayerEncounter.Current == null)
							{
								PlayerEncounter.Start();
								PlayerEncounter.Current.SetupFields(MobileParty.MainParty.Party, target_party.Party);
								Campaign.Current.CurrentConversationContext = ConversationContext.Default;
								HIValues.Instance.Created_Encounter = PlayerEncounter.Current;
								Campaign.Current.TimeControlMode = CampaignTimeControlMode.Stop;
								AccessTools.Field(typeof(PlayerEncounter), "_mapEventState").SetValue(PlayerEncounter.Current, PlayerEncounterState.Begin);
								AccessTools.Field(typeof(PlayerEncounter), "_stateHandled").SetValue(PlayerEncounter.Current, true);
								AccessTools.Field(typeof(PlayerEncounter), "_meetingDone").SetValue(PlayerEncounter.Current, true);

								ConversationCharacterData playerCharacterData = new ConversationCharacterData(CharacterObject.PlayerCharacter, MobileParty.MainParty.Party, false, false, false, false);
								ConversationCharacterData conversationPartnerData = new ConversationCharacterData(target_party.Party.Leader, target_party.Party, false, false, false, false);
								CampaignMission.OpenConversationMission(playerCharacterData, conversationPartnerData, "", "");
								return;
							}

							// open encylopedia if possible
							Hero leader = target_party.Party.Leader.HeroObject;
							if (Input.IsKeyDown(HIValues.Instance.CourtRecord) && leader != null)
							{
								Campaign.Current.EncyclopediaManager.GoToLink(leader.EncyclopediaLink.ToString());
							}
						}
					}
				}
				// no longer following a party
				else if (HIValues.Instance.MsgPrinted)
				{
					HIValues.Instance.PlayerTargetParty = null;
					HIValues.Instance.MsgPrinted = false;
				}
			}
		}
	}
}
